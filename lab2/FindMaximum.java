public class FindMaximum {

	public static void main(String[] args){
/*	    if (args.length> 0) {
            System.out.printf(args[0]); ///cmd de verdiğimizi de yazdırıyor>>>> java FindMaximum hello >>>> hello4
        }*/
        if (args.length==2) {
            int value1 = Integer.parseInt(args[0]);//stringi integera çevirdik
            int value2 = Integer.parseInt(args[1]);//stringi integera çevirdik
            int result;

            boolean someCondition = value1 > value2;

            result = someCondition ? value1 : value2;

            System.out.println(result);
        }
	}
}