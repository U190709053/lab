public class MyDate {

    private int day,month, year; //Jan is 0 ..... Dec 11

    int[] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month-1;
        this.year = year;
    }

    public void incrementDay() {
        int newDay = day+1;

        int maxDay = maxDays[month];

        if (newDay > maxDay){
            incrementMonth();
            day = 1;
        }else if (month == 1 && newDay == 29 && !leapYear()){
            day = 1;
            incrementMonth();
        }else {
            day = newDay;
        }
    }

    private boolean leapYear(){

        return year % 4 == 0 ? true:false;
    }
    public void incrementYear(int diff) {
        year += diff;
        if (month == 1 && day == 29 && !leapYear()){
            day = 28;
        }
    }

    public void decrementDay() {
        int newDay = day - 1;
        if (newDay == 0){
            day = 31;
            decrementMonth();
        }else {
            day = newDay;
        }
    }

    public void decrementYear() {
        incrementYear(-1);
    }

    public void decrementMonth() {
        incrementMonth(-1);
    }

    public void incrementDay(int diff) {
        while ( diff > 0){
            incrementDay();
            diff--;
        }
    }

    public void decrementMonth(int month) {
        incrementMonth(-month);
    }

    public void decrementDay(int diff) {
        while ( diff > 0) {
            decrementDay();
            diff--;
        }
    }

    public void incrementMonth(int diff) {
        int newMonth = (month+diff) % 12;
        int yearDiff = 0;

        if (newMonth < 0){
            newMonth +=12;
            yearDiff = -1;
        }
        yearDiff += (month+diff)/12;

        month = newMonth;
        year += yearDiff;

        if (day > maxDays[month]){
            day = maxDays[month];
            if (month == 1 && day == 29 && !leapYear()){
                day = 28;
            }
        }
    }

    public void decrementYear(int year) {
        incrementYear(-year);
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public boolean isBefore(MyDate anotherDate) {
        boolean isBefore = true;
        if (year < anotherDate.year) {
            isBefore = true;
        } else if (year > anotherDate.year) {
            isBefore = false;
        }
        if (year == anotherDate.year) {
            if (month < anotherDate.month) {
                isBefore = true;
            } else if (month > anotherDate.month) {
                isBefore = false;
            }
        }
        if (year == anotherDate.year && month == anotherDate.month) {
            if (day < anotherDate.day) {
                isBefore = true;
            } else if (day > anotherDate.day) {
                isBefore = false;
            }
        }
        return (isBefore);
    }

    public boolean isAfter(MyDate anotherDate) {
        boolean isAfter = true;
        if (year > anotherDate.year) {
            isAfter = true;
        } else if (year < anotherDate.year) {
            isAfter = false;
        }
        if (year == anotherDate.year) {
            if (month > anotherDate.month) {
                isAfter = true;
            } else if (month < anotherDate.month) {
                isAfter = false;
            }
        }
        if (year == anotherDate.year && month == anotherDate.month) {
            if (day > anotherDate.day) {
                isAfter = true;
            } else if (day < anotherDate.day) {
                isAfter = false;
            }
        }
        return (isAfter);
    }

    public int dayDifference(MyDate anotherDate) {
        int yearDiff = year - anotherDate.year;
        int monthDiff = month - anotherDate.month;
        int dayDiff = day - anotherDate.day;
        int monthsDays = 0;
        int totalDayDiff = 0;
        while (monthDiff != 0){
            monthsDays += maxDays[anotherDate.month];
            if (monthDiff<0){monthDiff++; }
            if (monthDiff>0){monthDiff--; }
        }
        totalDayDiff = (yearDiff*365)+monthsDays+dayDiff;
        if (year % 4 != 0 ) { totalDayDiff=totalDayDiff-1;
        }else if (anotherDate.year % 4 != 0 ){totalDayDiff=totalDayDiff-1;}
        return totalDayDiff;
    }
    public String toString(){
        return year + "-" + ((month+1) < 10 ? "0" :"") + (month+1) + "-" + (day < 10 ? "0" :"") + day;
    }
}
